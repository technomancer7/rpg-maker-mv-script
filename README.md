# RPG Maker MV Script

My generic script file for RMMV.<br>
Contains various functions I use for my projects.<br>
Will be regularly updated and expanded, with the intention of it never conflicting with anything else, always going to be
self-contained modular functions.<br>
<br>
Everything shown now is just my initial version, this is nowhere near complete to my intended vision.<br>

## Stealing
Super simple stealing script.<br>
Any skill you want to have an item steal effect, give it a Common Event trigger which calls the script:<br>
`Technomancer.Abilities.doSteal()`.<br>
Then, any enemy you want to have an item to steal should have at least the following in the note box: <br>
`<steal:(steal code)>`<br>
steal code refers to the format of the string that defines what is stolen, format of abbreviation of the type followed by the id, for example;<br>
`<steal:i2>` steals the item at id 2 in the database.<br>
`<steal:w3>` for weapons.<br>
`<steal:a2>` for armors.<br>
`<steal:g5000>` for money, same format but the number refers to the amount of money to steal. <br>
Module property can define the default steal odds, 0 to 100.<br>
For specific enemies having their own odds, you can add `<steal_odds:50>` to their notes.

### Note
If the skill does nothing but do a common event, it'll show a text box saying enemy wasn't effected. <br>
I don't know how to disable this yet. The ability still works, but it's kinda silly for the game to say the enemy wasn't effected, then showing that an item was stolen.<br>
A workaround for now is just make the ability give a state, even if its a nulled state that does nothing. That will disable the text box.

## Scan
Plug-n-play scan ability.<br>
Shows enemy information (Basic for now, will expand later)<br>
Same setup as stealing, but the script call is `Technomancer.Abilities.doScan()`.<br>
Whatever the ability is cast on, their name, hp, mp, (and if they have a steal tag, it'll show they have something), is shown in a text box.

### Note
Same as Stealing.


## Cheat functions
Gives all items to party: `Technomancer.Cheats.giveAll()`


## Todo
- [ ] More cheat functions to help with debugging
- [ ] Additional combat systems
  - [ ] Tactical Battle
     - [ ] Party Members placed on the field as events automatically
        - [ ] Iterate over current party, spawn events duplicating each
     - [ ] Player controls a cursor that activates the field events
     - [ ] Switch for check if battle is in progress, for use in disabling certain events while in battle
  - [ ] Duel battle system
    - [ ] Suikoden inspired, actor vs actor
    - [ ] RPS-style
    - [ ] Use of tags in the actor to define quotes that relate to an action
- [ ] Whatever else I think of at the time.

