//=============================================================================
// KaiserStuff.js
//=============================================================================

/*:
 * @plugindesc Various utilities and functions.
 * @author Kaiser
 *
 * @help This plugin does not provide plugin commands.
 * 
 * @param Steal Default Odds
 * @desc Default chance of stealing.
 * @default 100
 */

var Technomancer = Technomancer || {};  
Technomancer.Abilities = Technomancer.Abilities || {};  
Technomancer.Cheats = Technomancer.Cheats || {};  
(function() {
Technomancer.Cheats.giveAll = function(){
    $dataItems.forEach(function(item) {$gameParty.gainItem(item,99,false);});
    $dataArmors.forEach(function(armor){ $gameParty.gainItem(armor,1,false);});
    $dataWeapons.forEach(function(weapon){ $gameParty.gainItem(weapon,1,false);});
}

Technomancer.Abilities.doScan = function(){
    var en = $gameTroop._enemies[BattleManager._action._targetIndex];
    var id = en._enemyId;
    var stealstr = ""
    if(en.stolen != true) stealstr = `\nIs holding something.`
    
    var body = `  \\c[2]${en.originalName()}\\c[0]
    \\i[84] \\c[2]${en.hp}\\c[0]/\\c[2]${en.mhp}\\c[0]
    \\i[87] \\c[2]${en.mp}\\c[0]/\\c[2]${en.mmp}\\c[0] ${stealstr}`
    $gameMessage.add(body);
}

Technomancer.Abilities.doSteal = function(){
    var odds = Number(PluginManager.parameters('KaiserStuff')["Steal Default Odds"]);
    console.log(`def ${odds}`);
    
    var en = $gameTroop._enemies[BattleManager._action._targetIndex]
    var ss = en.enemy().meta["steal"]

    var new_odds = en.enemy().meta["steal_odds"]
    if(new_odds != undefined){
        odds = Number(new_odds);
        console.log(`Updated odds to ${odds}`)
    }


    if(en.stolen){ $gameMessage.add("Already stolen!"); return; }
    if (ss){
        var st = ss[0];
        var si = Number(ss.slice(1))
        if(st == "i"){
            var item = $dataItems[si]
            $gameMessage.add("Stole item: \\i["+item.iconIndex+"]"+item.name)
            $gameParty.gainItem(item, 1, false);
            en.stolen = true;
        } //item
        if(st == "a"){
            var item = $dataArmors[si]
            $gameMessage.add("Stole equipment: \\i["+item.iconIndex+"]"+item.name)
            $gameParty.gainItem($dataArmors[si], 1, false);
            en.stolen = true;
        } //armor
        if(st == "w"){
            var item = $dataWeapons[si]
            $gameMessage.add("Stole weapon: \\i["+item.iconIndex+"]"+item.name)
            $gameParty.gainItem($dataWeapons[si], 1, false);
            en.stolen = true;
        } //weapon
        if(st == "g"){
            $gameMessage.add("Stole money: "+si);
            $gameParty.gainGold(si);
            en.stolen = true;
        } //gold
    }else{
        $gameMessage.add(en.originalName()+" has nothing to steal.")
    }

}
})();
